package com.example.uapv1702371.tp1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        final Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");

        ArrayList<String> ArList=bundle.getStringArrayList("arlist");
        String name=ArList.get(0);
        String capital=ArList.get(1);
        String img=ArList.get(2);
        String language=ArList.get(3);
        String currency=ArList.get(4);
        String population=ArList.get(5);
        String area=ArList.get(6);

        final TextView nameV = (TextView)findViewById(R.id.nameCountry);
        nameV.setText(name);

        final EditText capitalText = (EditText)findViewById(R.id.capitalInput);
        capitalText.setText(capital);


        final EditText languageText = (EditText)findViewById(R.id.languageInput);
        languageText.setText(language);



        final EditText currencyText = (EditText)findViewById(R.id.currencyInput);
        currencyText.setText(currency);


        final EditText populationText = (EditText)findViewById(R.id.populationInput);
        populationText.setText(population);


        final EditText areaText = (EditText)findViewById(R.id.areaInput);
        areaText.setText(area);


        ImageView imgView = (ImageView)findViewById(R.id.imgFlag);
        imgView.setImageResource(getResources().getIdentifier(img,"drawable",getPackageName()));

        final Button button = findViewById(R.id.save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> ArString=new ArrayList<String>();
                ArString.add(nameV.getText().toString());
                ArString.add(capitalText.getText().toString());
                ArString.add(languageText.getText().toString());
                ArString.add(currencyText.getText().toString());
                ArString.add(populationText.getText().toString());
                ArString.add(areaText.getText().toString());

                Bundle B=new Bundle();
                B.putStringArrayList("ArString",ArString);
                intent.putExtra("data", B);
                setResult(Activity.RESULT_OK, intent);
                finish();

            }
        });

    }
}
