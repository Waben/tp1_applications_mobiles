package com.example.uapv1702371.tp1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final CountryList CL = new CountryList();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final ListView listview  = (ListView) findViewById(R.id.lstView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,CL.getNameArray());
        listview.setAdapter(adapter);

        final Intent intent = new Intent(this, CountryActivity.class);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                final String item = (String) parent.getItemAtPosition(position);
                Country C=CL.getCountry(item);
                ArrayList<String> ArString=new ArrayList<String>();
                Bundle B=new Bundle();
                ArString.add(item);
                ArString.add(C.getmCapital());
                ArString.add(C.getmImgFile());
                ArString.add(C.getmLanguage());
                ArString.add(C.getmCurrency());
                ArString.add(Integer.toString(C.getmPopulation()));
                ArString.add(Integer.toString(C.getmArea()));

                B.putStringArrayList("arlist",ArString);
                intent.putExtra("data", B);

                startActivityForResult(intent, 1);

            }
            });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == 1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Bundle B = data.getBundleExtra("data");
                ArrayList<String> ArList = B.getStringArrayList("ArString");
                Country C = CL.getCountry(ArList.get(0));
                C.setmCapital(ArList.get(1));
                C.setmLanguage(ArList.get(2));
                C.setmCurrency(ArList.get(3));
                C.setmPopulation(Integer.parseInt(ArList.get(4)));
                C.setmArea(Integer.parseInt(ArList.get(5)));
            }
        }

    }
}
